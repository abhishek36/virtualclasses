const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const User = new Schema(
  {
    deviceToken: {
      type: String,
    },
    username: {
      type: String,
    },
    roll: {
      type: Number,
    },
    age: {
      type: Number,
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    category: {
      type: String,
    },
    religion: {
      type: String,
    },
    caste: {
      type: String,
    },
    gender: {
      type: String,
      enum: ["Male", "Female"],
    },
    dob: {
      type: String,
    },
    addmissionDate: {
      type: String,
    },
    addmissionNo: {
      type: String,
    },
    medical: {
      type: String,
    },
    image: {
      type: String,
    },
    socialNetworks: {
      linkedIn: String,
      facebook: String,
      twitter: String,
      instagram: String,
    },
    accessToken: String,
    refreshToken: String,
    mobileNumber: {
      type: String,
      unique: true,
    },
    email: {
      type: String,
      unique: true,
    },
    encryptedEmail: {
      type: String,
    },
    designation: {
      type: String,
      enum: [
        "Super Admin",
        "Admin",
        "Student",
        "Teacher",
        "Class",
        "Parent",
        "Father",
        "Mother",
        "Guardian",
      ],
    },
    classId: {
      type: mongoose.Types.ObjectId,
      ref: "Class",
    },
    section: {
      type: String,
    },
    loginType: {
      type: String,
      enum: ["Google", "Facebook", "Password", "OTP"],
      default: "Password",
      required: true,
    },
    isMobileVerified: {
      type: String,
      enum: ["Not", "Verified"],
      default: "Not",
      required: true,
    },
    otp: {
      type: String,
    },
    isEmailVerified: {
      type: String,
      enum: ["Not", "Verified"],
      default: "Not",
      required: true,
    },
    address: {
      type: String,
    },
    // location: {
    //   address: String,
    //   landmark: String,
    //   state: String,
    //   city: String,
    //   pincode: Number,
    //   country: String,
    //   lat: {
    //     type: String,
    //     require: true,
    //   },
    //   lng: {
    //     type: String,
    //     require: true,
    //   },
    // },
    password: {
      type: String,
    },
    height: {
      type: String,
    },
    weight: {
      type: String,
    },
    status: {
      type: String,
      default: "active",
    },
    isParentId: {
      type: mongoose.Types.ObjectId,
      ref: "ParentDetails",
    },
    isDetailId: {
      type: mongoose.Types.ObjectId,
      ref: "Details",
    },
    addedBy: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
    subscription: String,
    validity: {
      fromDate: String,
      toDate: String,
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("User", User);
